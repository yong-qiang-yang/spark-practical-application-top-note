 1.Spark Core之RDD编程中RDD的创建
   
   1).SparkContext
   SparkContext是编写Spark程序用到的第一个类，是Spark的主要入口点，它负责和
整个集群的交互;
   如把Spark集群当作服务端，那么Driver就是客户端，SparkContext 是客户端的核心；
   SparkContext是Spark的对外接口，负责向调用者提供 Spark 的各种功能；
   SparkContext用于连接Spark集群、创建RDD、累加器、广播变量；
   在 spark-shell 中 SparkContext 已经创建好了，可直接使用；
   编写Spark Driver程序第一件事就是：创建SparkContext;
   建议：Standalone模式或本地模式学习RDD的各种算子；
   不需要HA；不需要IDEA
   2).从集合创建RDD
   从集合中创建RDD，主要用于测试。Spark 提供了以下函数：parallelize、makeRDD、
range

val rdd1 = sc.parallelize(Array(1,2,3,4,5))
val rdd2 = sc.parallelize(1 to 100)
// 检查 RDD 分区数
rdd2.getNumPartitions
rdd2.partitions.length
// 创建 RDD，并指定分区数
val rdd2 = sc.parallelize(1 to 100)
rdd2.getNumPartitions

val rdd3 = sc.makeRDD(List(1,2,3,4,5))
rdd3.collect
val rdd4 = sc.makeRDD(1 to 100)
rdd4.getNumPartitions

val rdd5 = sc.range(1, 100, 3)
rdd5.getNumPartitions
val rdd6 = sc.range(1, 100, 2 ,10)
rdd6.getNumPartitions
   
   备注：rdd.collect 方法在生产环境中不要使用，会造成Driver OOM
   3).从文件系统创建RDD
   用 textFile() 方法来从文件系统中加载数据创建RDD。方法将文件的 URI 作为参数，
这个URI可以是：
          本地文件系统
          使用本地文件系统要注意：该文件是不是在所有的节点存在(在Standalone
模式下)
          分布式文件系统HDFS的地址
           Amazon S3的地址

// 从本地文件系统加载数据
val lines = sc.textFile("file:///root/data/wc.txt")
// 从分布式文件系统加载数据
val lines =sc.textFile("hdfs://linux121:9000/user/root/data/uaction.dat")
val lines = sc.textFile("/user/root/data/uaction.dat")
val lines = sc.textFile("data/uaction.dat")
val lines =sc.textFile("hdfs://linux121:9000/wcinput/wc.txt")
   4).从RDD创建RDD
   本质是将一个RDD转换为另一个RDD。详细信息参见 3.5 Transformation