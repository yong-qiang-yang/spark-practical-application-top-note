 1.大数据高速计算引擎Spark（上）之Spark Core中RDD编程下输入与输出
   
   1).文件输入与输出
   (1).文本文件
   数据读取：textFile(String)。可指定单个文件，支持通配符。
   这样对于大量的小文件读取效率并不高，应该使用 wholeTextFiles
   def wholeTextFiles(path: String, minPartitions: Int = defaultMinPartitions):
RDD[(String, String)])
   返回值RDD[(String, String)]，其中Key是文件的名称，Value是文件的内容
   数据保存：saveAsTextFile(String)。指定的输出目录。
   (2).csv文件
   读取 CSV（Comma-Separated Values）/TSV（Tab-Separated Values） 数据和读取 
JSON 数据相似，都需要先把文件当作普通文本文件来读取数据，然后通过将每一行进
行解析实现对CSV的读取。

   CSV/TSV 数据的输出也是需要将结构化RDD通过相关的库转换成字符串RDD，然后使用
Spark 的文本文件 API 写出去。
   (3).json文件
   如果 JSON 文件中每一行就是一个JSON记录，那么可以通过将JSON文件当做文本文
件来读取，然后利用相关的JSON库对每一条数据进行JSON解析。

   JSON数据的输出主要是通过在输出之前将由结构化数据组成的 RDD 转为字符串RDD，
然后使用 Spark 的文本文件 API 写出去。
   
   json文件的处理使用SparkSQL最为简洁。
   (4).SequenceFile
   SequenceFile文件是Hadoop用来存储二进制形式的key-value对而设计的一种平面文件
(Flat File)。Spark 有专门用来读取 SequenceFile 的接口。在 SparkContext中，可以
调用：sequenceFile[keyClass, valueClass]；
   调用 saveAsSequenceFile(path) 保存PairRDD，系统将键和值能够自动转为Writable类型。
   (5).对象文件
   对象文件是将对象序列化后保存的文件，采用Java的序列化机制。
   通过objectFile[k,v](path) 接收一个路径，读取对象文件，返回对应的 RDD，也可以通过
调用saveAsObjectFile() 实现对对象文件的输出。因为是序列化所以要指定类型。
   2).JDBC
   详见综合案例