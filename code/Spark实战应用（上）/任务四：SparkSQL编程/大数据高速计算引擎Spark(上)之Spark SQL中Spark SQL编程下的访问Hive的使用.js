 1.大数据高速计算引擎Spark(上)之Spark SQL中Spark SQL编程下的访问Hive
   
   在 pom 文件中增加依赖：
<dependency>
      <groupId>org.apache.spark</groupId>
      <artifactId>spark-hive_2.12</artifactId>
      <version>${spark.version}</version>
</dependency>
   
   在 resources中增加hive-site.xml文件，在文件中增加内容：
<configuration>
    <property>
        <name>hive.metastore.uris</name>
        <value>thrift://linux123:9083</value>
    </property>
</configuration>

   备注：最好使用 metastore service 连接Hive；使用直连 metastore 的方式时，
SparkSQL程序会修改 Hive 的版本信息；
   默认Spark使用 Hive 1.2.1 进行编译，包含对应的serde, udf, udaf等。
   
package cn.lagou.sparksql

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

object AccessHive {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Demo1")
      .master("local[*]")
      .enableHiveSupport()
      // 设为true时，Spark使用与Hive相同的约定来编写Parquet数据
      .config("spark.sql.parquet.writeLegacyFormat", true)
      .getOrCreate()

    val sc = spark.sparkContext
    sc.setLogLevel("warn")

    spark.sql("show databases").show
    spark.sql("select * from ods.ods_trade_product_info").show
    val df: DataFrame = spark.table("ods.ods_trade_product_info")
    df.show()
    df.write.mode(SaveMode.Append).saveAsTable("ods.ods_trade_product_info_backup")
    spark.table("ods.ods_trade_product_info_backup").show

    spark.close()
  }

}
