 1.大数据高速计算引擎Spark(上)之Spark Core中Spark概述
   
   课程内容：
       Spark Core -- 离线
       Spark SQL -- 离线、交互
       Spark Streaming -- 实时
       Spark GraphX -- 图处理
       Spark原理
   MapReduce、Spark、Flink（实时） => 3 代计算引擎；昨天、今天、未来
   MapReduce、Spark：类MR的处理引擎；底层原理非常相似；数据分区、maptask、reduce 
task、shuffle
   
   1).什么是Spark
   Spark是当今大数据领域最活跃、最热门、最高效的大数据通用计算引擎
   2009 年诞生于美国加州大学伯克利分校AMP 实验室
   2010 年通过BSD许可协议开源发布
   2013 年捐赠给Apache软件基金会并切换开源协议到切换许可协议至 Apache2.0
   2014 年2月，Spark 成为 Apache 的顶级项目
   2014 年11月, Spark的母公司Databricks团队使用Spark刷新数据排序世界记录
   Spark 成功构建起了一体化、多元化的大数据处理体系。在任何规模的数据计算中，
Spark 在性能和扩展性上都更具优势
   
   Spark 是一个快速、通用的计算引擎。Spark的特点：
      速度快。与 MapReduce 相比，Spark基于内存的运算要快100倍以上，基于硬
盘的运算也要快10倍以上。Spark实现了高效的DAG执行引擎，可以通过基于内存来高效
处理数据流；
      使用简单。Spark支持 Scala、Java、Python、R的API，还支持超过80种高级算
法，使用户可以快速构建不同的应用。而且Spark支持交互式的Python和Scala的shell，
可以非常方便地在这些shell中使用Spark集群来验证解决问题的方法；
      通用。Spark提供了统一的解决方案。Spark可以用于批处理、交互式查询
(Spark SQL)、实时流处理(Spark Streaming)、机器学习(Spark MLlib)和图计算解决具
(GraphX)。这些不同类型的处理都可以在同一个应用中无缝使用。Spark统一的方案非常
有吸引力，企业想用统一的平台去处理遇到的问题，减少开发和维护的人力成本和部署
平台的物力成本；
      兼容好。Spark可以非常方便地与其他的开源产品进行融合。Spark可以使用YARN、
Mesos作为它的资源管理和调度器；可以处理所有Hadoop支持的数据，包括HDFS、HBase和
Cassandra等。这对于已经部署Hadoop集群的用户特别重要，因为不需要做任何数据迁移
就可以使用Spark的强大处理能力。Spark也可以不依赖于第三方的资源管理和调度器，它
实现了Standalone作为其内置的资源管理和调度框架，这样进一步降低了Spark的使用门槛，
使得所有人都可以非常容易地部署和使用Spark。此外，Spark还提供了在EC2上部署Standalone
的Spark集群的工具。
   2).Spark 与 Hadoop
   从狭义的角度上看：Hadoop是一个分布式框架，由存储、资源调度、计算三部分组成；
   
   Spark是一个分布式计算引擎，由 Scala 语言编写的计算框架，基于内存的快速、通用、
可扩展的大数据分析引擎；
   
   从广义的角度上看，Spark是Hadoop生态中不可或缺的一部分；
   
   MapReduce的不足：
      表达能力有限
      磁盘IO开销大
      延迟高
	      任务之间的衔接有IO开销
          在前一个任务执行完成之前，后一个任务无法开始。难以胜任复杂的、多阶段
计算任务
   
   Spark在借鉴MapReduce优点的同时，很好地解决了MapReduce所面临的问题。
   MapReduce                    Spark
   数据存储结构：磁盘           使用内存构建弹性分布式数据
   HDFS文件系统的split          集RDD对数据进行运算和cache
   编程范式：Map + Reduce仅     提供了丰富的操作，使数据
   提供两个操作，表达力欠缺     处理逻辑的代码非常简短
   计算中间结果落到磁盘，IO     计算中间结果在内存中，维护
   及序列化、反序列化代价大     存取速度比磁盘高几个数量级
   Task以进程的方式维护，需     Task以线程的方式维护对于小数
   要数秒时间才能启动任务       据集读取能够达到亚秒级的延迟
   
   备注：Spark的计算模式也属于MapReduce；Spark框架是对MR框架的优化；
   
   在实际应用中，大数据应用主要包括以下三种类型：
       批量处理（离线处理）：通常时间跨度在数十分钟到数小时之间
       交互式查询：通常时间跨度在数十秒到数分钟之间
	   流处理（实时处理）：通常时间跨度在数百毫秒到数秒之间
   
   当同时存在以上三种场景时，传统的Hadoop框架需要同时部署三种不同的软件。如：
       MapReduce / Hive 或 Impala / Storm
   
   这样做难免会带来一些问题：
       不同场景之间输入输出数据无法做到无缝共享，通常需要进行数据格式的转换
       不同的软件需要不同的开发和维护团队，带来了较高的使用成本
       比较难以对同一个集群中的各个系统进行统一的资源协调和分配
   Spark所提供的生态系统足以应对上述三种场景，即同时支持批处理、交互式查询和
流数据处理：
       Spark的设计遵循“一个软件栈满足不同应用场景”的理念（all in one），逐渐形
成了一套完整的生态系统
       既能够提供内存计算框架，也可以支持SQL即席查询、实时流式计算、机器学习和
图计算等
       Spark可以部署在资源管理器YARN之上，提供一站式的大数据解决方案
   
   Spark 为什么比 MapReduce 快：
   (1).Spark积极使用内存。MR框架中一个Job包括一个map 阶段(一个或多个map task)
和一个 reduce阶段(一个或多个reduce Task)。如果业务处理逻辑复杂，此时需要将多
个job 组合起来；然而前一个job的计算结果必须写到HDFS，才能交给后一个job。这样
一个复杂的运算,在MR框架中会发生很多次写入、读取操作；Spark框架可以把多个map 
reduce task组合在一起连续执行，中间的计算结果不需要落地；
   复杂的MR任务：mr + mr + mr + mr +mr ...
   复杂的Spark任务：mr -> mr -> mr ......
   (2).多进程模型(MR) vs 多线程模型(Spark)。MR框架中的的Map Task和Reduce Task
是进程级别的，而Spark Task是基于线程模型的。MR框架中的 map task、reduce task
都是jvm进程,每次启动都需要重新申请资源，消耗了不必要的时间。Spark则是通过复用
线程池中的线程来减少启动、关闭task所需要的系统开销。
   3).系统架构
   Spark运行架构包括：
       Cluster Manager
       Worker Node
       Driver
       Executor
   Cluster Manager 是集群资源的管理者。Spark支持3种集群部署模式：
   Standalone、Yarn、Mesos；
   
   Worker Node 工作节点，管理本地资源；

   Driver Program。运行应用的main()方法并且创建了SparkContext。由Cluster
Manager分配资源，SparkContext 发送 Task 到 Executor 上执行；
   
   Executor：在工作节点上运行，执行Driver发送的Task，并向Driver汇报计算结果；
   4).Spark集群部署模式
   Spark支持3种集群部署模式：Standalone、Yarn、Mesos；
   (1).Standalone模式
       独立模式，自带完整的服务，可单独部署到一个集群中，无需依赖任何其他资源
管理系统。从一定程度上说，该模式是其他两种的基础
       Cluster Manager：Master
	   Worker Node：Worker
       仅支持粗粒度的资源分配方式
   (2).Spark On Yarn模式
       Yarn拥有强大的社区支持，且逐步已经成为大数据集群资源管理系统的标准
       在国内生产环境中运用最广泛的部署模式
       Spark on yarn 的支持两种模式：
	       yarn-cluster：适用于生产环境
           yarn-client：适用于交互、调试，希望立即看到app的输出
       Cluster Manager：ResourceManager
       Worker Node：NodeManager
       仅支持粗粒度的资源分配方式
   (3).Spark On Mesos模式
       官方推荐的模式。Spark开发之初就考虑到支持Mesos
       Spark运行在Mesos上会比运行在YARN上更加灵活，更加自然
       Cluster Manager：Mesos Master
       Worker Node：Mesos Slave
       支持粗粒度、细粒度的资源分配方式
   
   粗粒度模式（Coarse-grained Mode）：每个应用程序的运行环境由一个Dirver和
若干个Executor组成，其中，每个Executor占用若干资源，内部可运行多个Task。应用
程序的各个任务正式运行之前，需要将运行环境中的资源全部申请好，且运行过程中要
一直占用这些资源，即使不用，最后程序运行结束后，回收这些资源。
   
   细粒度模式（Fine-grained Mode）：鉴于粗粒度模式会造成大量资源浪费，Spark
On Mesos还提供了另外一种调度模式：细粒度模式，这种模式类似于现在的云计算，
核心思想是按需分配。
   
   三种集群部署模式如何选择：
       生产环境中选择Yarn，国内使用最广的模式
       Spark的初学者：Standalone，简单
       开发测试环境，可选择Standalone
       数据量不太大、应用不是太复杂，建议可以从Standalone模式开始
       mesos不会涉及到
   5).相关术语
   http://spark.apache.org/docs/latest/cluster-overview.html
      Application
	  用户提交的spark应用程序,由集群中的一个driver 和许多executor组成
	  Application jar
	  一个包含spark应用程序的jar,jar不应该包含Spark或Hadoop的jar，这些jar应该在
运行时添加
      Driver program
	  运行应用程序的main()，并创建SparkContext(Spark应用程序)
      Cluster manager
	  管理集群资源的服务，如standalone，Mesos，Yarn
      Deploy mode
	  区分driver进程在何处运行。在Cluster模式下,在集群内部运行Driver。 在Client模式下，
Driver 在集群外部运行
      Worker node
	  运行应用程序的工作节点
      Executor
	  运行应用程序Task和保存数据,每个应用程序都有自己的executors，并且各个executor相互独立
      Task
      executors应用程序的最小运行单元
	  Job
      在用户程序中，每次调用Action函数都会产生一个新的job，也就是说每个Action生成
一个job
      Stage
      一个job被分解为多个stage，每个stage是一系列Task的集合