package cn.lagou.homework

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object SparkDemo {
  def main(args: Array[String]): Unit = {
    // 1.创建SparkContext
    val conf = new SparkConf().setAppName(this.getClass.getCanonicalName.init).setMaster("local[*]")
    val sc = new SparkContext(conf)
    sc.setLogLevel("WARN")

    val clickLog: RDD[String] = sc.textFile("data/click.log")
    val impLog: RDD[String] = sc.textFile("data/imp.log")

    // 读文件：点击日志
    val clickRDD: RDD[(String, (Int, Int))] = clickLog.map { line =>
      val arr: Array[String] = line.split("\\s+")
      val adid: String =
        arr(3).substring(arr(3).lastIndexOf("=") + 1)
      (adid, (1, 0))
    }

    // 读文件：曝光日志
    val impRDD: RDD[(String, (Int, Int))] = impLog.map { line =>
      val arr: Array[String] = line.split("\\s+")
      val adid: String =
        arr(3).substring(arr(3).lastIndexOf("=") + 1)
      (adid, (0, 1))
    }

    // join
    val RDD: RDD[(String, (Int, Int))] = clickRDD.union(impRDD)
      .reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))

    RDD.foreach(println)
    // 写hdfs
    RDD.saveAsTextFile("hdfs://linux121:9000/data/")
    sc.stop()
  }

}
